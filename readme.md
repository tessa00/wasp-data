<h2> WASP - Web application supporting SNOMED CT Postcoordination</h2>

The postcoordination of concepts is often avoided due to its complexity and lack of software support, although this is exactly what makes SNOMED CT a flexible and powerful interlingua. Therefore, a web application called [WASP](https://wasp.imi.uni-luebeck.de/) was developed to support the creation of postcoordinated SNOMED CT expressions (PCE). The syntactic and semantic rules specified by SNOMED CT ([Compositional Grammar](https://confluence.ihtsdotools.org/display/DOCSCG/Compositional+Grammar+-+Specification+and+Guide) and [Concept Model](https://confluence.ihtsdotools.org/display/DOCGLOSS/SNOMED+CT+concept+model)) are ensured. This work was published in a 2023 paper in [Applied Sciences](https://www.mdpi.com/2076-3417/13/10/6114).

To use WASP, it is necessary to download this folder. This contains the folder ["wasp"](https://gitlab.com/tessa00/wasp-data), and must be located locally on the user's computer:

```
somewhere-in-the-file-system
└─── wasp
    └─── conceptModelObjectAttributes.csv
    └─── mrcm.json
    └─── server.json
    └─── templates
        └─── template_1.json
        └─── ...
        └─── template_n.json
```

The individual files are briefly described below:
- [conceptModelObjectAttributes.csv](https://gitlab.com/tessa00/wasp-data/-/blob/main/wasp/conceptModelObjectAttributes.csv)
    - Contains all SNOMED CT attributes of the Concept Model as well as an explanation of these and the associated value ranges
- [mrcm.json](https://gitlab.com/tessa00/wasp-data/-/blob/main/wasp/mrcm.json)
    - Pre-processed MRCM (International Edition, version 2024-07-01, option to generate this file for different version in WASP) in JSON format, which is of central importance for the generation of the PCE without templates and for template generation
- [server.json](https://gitlab.com/tessa00/wasp-data/-/blob/main/wasp/server.json)
    - Contains the URL of the CSIRO Ontoserver by default, this file can be extended by further own terminology servers
- [templates](https://gitlab.com/tessa00/wasp-data/-/tree/main/wasp/templates)
    - contains by default the [templates of SNOMED International](https://github.com/IHTSDO/snomed-templates); own generated templates via WASP have to be copied manually into this folder

